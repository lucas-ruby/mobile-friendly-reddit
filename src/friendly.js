const defaults = {
    showAll: true,
    noAvatars: false,
    easySelect: false,
    hideAnnouncement: false
};

async function getPreferences() {
    return browser.storage.sync.get(defaults);
}

async function disableSelectionHeader() {
    document.querySelectorAll('.CommentHeader__timestamp').forEach(e => {
        e.addEventListener('selectstart', () => false);
        e.style['-moz-user-select'] = 'none';
    });
}

async function addPrefsToBody() {
    const preferences = await getPreferences();
    const args = Object.entries(preferences)
        .filter(([ _k, v ]) => v)
        .map(([ k, _v ]) => k.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase()); // camelCase to kebab-case
    document.body.classList.add(...args);
    if (preferences.easySelect === true) {
        disableSelectionHeader();
    }
}

window.requestAnimationFrame(() => addPrefsToBody());